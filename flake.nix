{
  description = "TODO: Find a clever description";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
      with pkgs;
      let
        revealjs = fetchFromGitHub {
          owner = "hakimel";
          repo = "reveal.js";
          rev = "5.1.0";
          hash = "sha256-L6KVBw20K67lHT07Ws+ZC2DwdURahqyuyjAaK0kTgN0=";
        };

        nativeBuildInputs = [
          pandoc
          qrencode
        ];

        default = stdenv.mkDerivation {
          inherit nativeBuildInputs;

          name = "presentation";
          src = ./.;

          installPhase = ''
            mkdir -p $out
            cp -r public $out/public
            cp -r ${revealjs} $out/public/reveal.js
          '';
        };
      in
      {
        formatter = nixfmt-rfc-style;

        devShells.default = mkShell {
          inherit nativeBuildInputs;

          shellHook = ''
            alias l="${eza}/bin/eza -lAh"

            mkdir -p public
            ln -s ${revealjs}/ public/reveal.js
            trap "rm -rf public" EXIT
          '';
        };

        packages = {
          inherit default;

          human_talk = stdenv.mkDerivation {
            name = "human_talk";
            src = ./.;

            buildPhase = ''
              echo "#!{bash}/bin/bash" >> $name
              echo "echo Hello, world" >> $name
            '';

            installPhase = ''
              mkdir -p $out/bin
              cp $name $out/bin/$name
              chmod +x $out/bin/$name
            '';
          };
        };
      }
    );
}
