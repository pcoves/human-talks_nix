.PHONY: build
build: src/index.md
	mkdir -p public
	cp -r img public/img
	qrencode https://gitlab.com/pcoves/human-talks_nix \
		-t SVG                                         \
		-s 5                                           \
		-m 2                                           \
		-o public/qrcode.svg
	pandoc src/index.md       			  \
		--output public/index.html        \
		--from markdown+emoji             \
		--to revealjs                     \
		--variable revealjs-url=reveal.js \
		--slide-level 4                   \
		--standalone


.PHONY: clean
clean:
	rm -rf public
