# Human Talks - `nix`

## Browse

[https://pcoves.gitlab.io/human-talks_nix/](https://pcoves.gitlab.io/human-talks_nix/)

## Build

```bash
git clone https://gitlab.com/pcoves/human-talks_nix.git
cd human-talks_nix
nix build
```

Your presentation awaits in `./result/public/index.html`.
